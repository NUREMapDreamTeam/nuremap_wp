﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NURE_map
{
    public class ClassRoom
    {
        public int VertexID { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public ClassRoom(string name, int id, string descr)
        {
            VertexID = id;
            Name = name;
            Description = descr;
        }
    }
}
