﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NURE_map
{
    class BuildingStructure
    {
        public class Corps
        {
            public string name;
            public int maxFloor;
            public int minFloor;
            public int k;
            public Corps(string name, int k)
            {
                this.name = name;
            }
        }
        public static Dictionary<string, Corps> info = new Dictionary<string, Corps>();
        public static void init()
        {
            info.Add("main", new Corps("main", 3));
            info.Add("i", new Corps("i", 3));
            info.Add("z", new Corps("z", 1));
            foreach (var i in Graph.verteces)
            {
                if (!info.ContainsKey(i.corp))
                    info.Add(i.corp, new Corps(i.corp,1));
                if (info[i.corp].minFloor > i.floor)
                    info[i.corp].minFloor = i.floor;
                if (info[i.corp].maxFloor < i.floor)
                    info[i.corp].maxFloor = i.floor;
                
            }
        }
    }
}
