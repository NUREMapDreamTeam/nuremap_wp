﻿using System;
using System.Windows;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Threading;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace NURE_map
{
    public partial class PathView : PhoneApplicationPage
    {
        const double MaxScale = 10;

        double _scale = 1.0;
        double _minScale;
        double _coercedScale;
        double _originalScale;

        Size _viewportSize;
        bool _pinching;
        Point _screenMidpoint;
        Point _relativeMidpoint;

        BitmapImage _bitmap; 
        public PathView()
        {
            InitializeComponent();

            path = global.path;

            polyline = new Polyline();
            polyline.StrokeLineJoin = PenLineJoin.Round;
            polyline.StrokeThickness = 8;
            polyline.Stroke = new SolidColorBrush(Color.FromArgb(255, 72, 44, 254));
            polyline.StrokeStartLineCap = PenLineCap.Round;
            polyline.StrokeEndLineCap = PenLineCap.Round;

            canvas.Children.Add(polyline);
            {
                startPointMark = new Ellipse();
                startPointMark.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                startPointMark.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                startPointMark.Fill = polyline.Stroke;
                startPointMark.StrokeThickness = 0;
                startPointMark.Width = startPointMark.Height = polyline.StrokeThickness * 2;
                canvas.Children.Add(startPointMark);
            }

            {
                endPointMark = new Ellipse();
                endPointMark.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                endPointMark.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                endPointMark.Fill = polyline.Stroke;
                endPointMark.StrokeThickness = 0;
                endPointMark.Width = endPointMark.Height = polyline.StrokeThickness * 2;
                canvas.Children.Add(endPointMark);
            }

            draw();
        }
        Path path;
        int lastf = 0;
        Polyline polyline;
        Ellipse startPointMark, endPointMark;
        Point startPoint;
        void draw()
        {
            if (lastf == path.parts.Length)
            {
                NavigationService.Navigate(new Uri("/Pages/MainPage.xaml", UriKind.Relative));
                return;
            }
            {
                TranslateTransform startCoords = new TranslateTransform();
                startCoords.X = path.lpoints[lastf][0].X - polyline.StrokeThickness;
                startCoords.Y = path.lpoints[lastf][0].Y - polyline.StrokeThickness;
                startPointMark.RenderTransform = startCoords;
            }
            {
                TranslateTransform endCoords = new TranslateTransform();
                endCoords.X = path.lpoints[lastf][path.lpoints[lastf].Count - 1].X - polyline.StrokeThickness;
                endCoords.Y = path.lpoints[lastf][path.lpoints[lastf].Count - 1].Y - polyline.StrokeThickness;
                endPointMark.RenderTransform = endCoords;
            }
            polyline.Points = path.lpoints[lastf];

            var floorBitmap = new BitmapImage(new Uri("/MapsImg/" + path.parts[lastf].Floor + path.parts[lastf].Corps + ".png", UriKind.RelativeOrAbsolute));
            //floorBitmap;
            mapImage.Source = floorBitmap;
            startPoint = path.lpoints[lastf][0];
            ++lastf;
            if (lastf == path.parts.Length)
                nextButton.Content = "завершить просмотр";
            else
                if (path.parts[lastf - 1].Corps != path.parts[lastf].Corps)
                    nextButton.Content = "перейти в корпус " + path.parts[lastf].Corps;
                else
                    if (path.parts[lastf - 1].Floor > path.parts[lastf].Floor)
                        nextButton.Content = "спуститься на " + path.parts[lastf].Floor + " этаж";
                    else
                        nextButton.Content = "подняться на " + path.parts[lastf].Floor + " этаж";
        }
        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            draw();
        }
        void viewport_ViewportChanged(object sender, System.Windows.Controls.Primitives.ViewportChangedEventArgs e)
        {
            Size newSize = new Size(viewport.Viewport.Width, viewport.Viewport.Height);
            if (newSize != _viewportSize)
            {
                _viewportSize = newSize;
                CoerceScale(true);
                ResizeImage(false);
            }
        }
        void OnManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            _pinching = false;
            _originalScale = _scale;
        }
        void OnManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (e.PinchManipulation != null)
            {
                e.Handled = true;

                if (!_pinching)
                {
                    _pinching = true;
                    Point center = e.PinchManipulation.Original.Center;
                    _relativeMidpoint = new Point(center.X / mapImage.ActualWidth, center.Y / mapImage.ActualHeight);

                    var xform = canvas.TransformToVisual(viewport);

                    _screenMidpoint = xform.Transform(center);
                }

                _scale = _originalScale * e.PinchManipulation.CumulativeScale;

                CoerceScale(false);
                ResizeImage(false);
            }
            else if (_pinching)
            {
                _pinching = false;
                _originalScale = _scale = _coercedScale;
            }
        }

        void OnManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            _pinching = false;
            _scale = _coercedScale;
        }

        void OnImageOpened(object sender, RoutedEventArgs e)
        {
            _bitmap = (BitmapImage)mapImage.Source;

            // Set scale to the minimum, and then save it. 
            _scale = 1;
            CoerceScale(true);
            _scale = _coercedScale;

            ResizeImage(true);
            viewport.SetViewportOrigin(
                       new Point(startPoint.X -_viewportSize.Width/2,
                           startPoint.Y - _viewportSize.Width/2));
        }

        void ResizeImage(bool center)
        {
            if (_coercedScale != 0 && _bitmap != null)
            {
                double newWidth = canvas.Width = Math.Round(_bitmap.PixelWidth * _coercedScale);
                double newHeight = canvas.Height = Math.Round(_bitmap.PixelHeight * _coercedScale);

                xform.ScaleX = xform.ScaleY = _coercedScale;

                viewport.Bounds = new Rect(0, 0, newWidth, newHeight);

                if (center)
                {
                    viewport.SetViewportOrigin(
                        new Point(
                            Math.Round((newWidth - viewport.ActualWidth) / 2),
                            Math.Round((newHeight - viewport.ActualHeight) / 2)
                            ));
                }
                else
                {
                    Point newImgMid = new Point(newWidth * _relativeMidpoint.X, newHeight * _relativeMidpoint.Y);
                    Point origin = new Point(newImgMid.X - _screenMidpoint.X, newImgMid.Y - _screenMidpoint.Y);
                    viewport.SetViewportOrigin(origin);
                }
            }
        }

        void CoerceScale(bool recompute)    //set min scale
        {
            if (recompute && _bitmap != null && viewport != null)
            {
                // Calculate the minimum scale to fit the viewport 
                double minX = viewport.ActualWidth / _bitmap.PixelWidth;
                double minY = viewport.ActualHeight / _bitmap.PixelHeight;

                _minScale = Math.Min(minX, minY);
            }

            _coercedScale = Math.Min(MaxScale, Math.Max(_scale, _minScale));    // minscale < scale < maxscale

        }
    }
}