﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;

namespace NURE_map
{
    public partial class ToSelect : PhoneApplicationPage
    {
        List<ClassRoom> source;
        public ToSelect()
        {
            InitializeComponent();

            source = new List<ClassRoom>();
            foreach (var j in Graph.classrooms.AsEnumerable())
                foreach (var i in Graph.classrooms[j.Key])
                    if (i.Key[0] != '_') source.Add(i.Value);
            TextBox_TextChanged(null, null);
            SearchField_LostFocus(null, null);
        }

        private void classRoomsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = (ClassRoom)(((LongListSelector)sender).SelectedItem) as ClassRoom;
            global.path = PathCreator.GetPath(global.from.VertexID, c.VertexID);
            
            NavigationService.Navigate(new Uri("/Pages/PathView.xaml", UriKind.Relative));
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<ClassRoom> SearchResult = new List<ClassRoom>();
            foreach (var i in source)
                if (i.Name.ToLower().StartsWith(SearchField.Text.ToLower()) ||
                    i.Description.ToLower().Contains(SearchField.Text.ToLower()))
                    SearchResult.Add(i);
            List<AlphaKeyGroup<ClassRoom>> DataSource = AlphaKeyGroup<ClassRoom>.CreateGroups(SearchResult,
                (ClassRoom s) => { return Graph.verteces[s.VertexID].corp; }, true);
            classRoomsList.ItemsSource = DataSource;
        }

        private void SearchField_GotFocus(object sender, RoutedEventArgs e)
        {
            SearchField.Text = "";
            TextBox_TextChanged(null, null);
            Header.Visibility = System.Windows.Visibility.Collapsed;
            SearchField.TextChanged -= new TextChangedEventHandler(TextBox_TextChanged);
            SearchField.Text = "";
            SearchField.Foreground.Opacity = 1;
            SearchField.TextChanged += new TextChangedEventHandler(TextBox_TextChanged);
        }

        private void SearchField_LostFocus(object sender, RoutedEventArgs e)
        {
            Header.Visibility = System.Windows.Visibility.Visible;
            if (SearchField.Text == "")
            {
                SearchField.TextChanged -= new TextChangedEventHandler(TextBox_TextChanged);
                SearchField.Text = "Поиск...";
                SearchField.Foreground.Opacity = 0.5;
                SearchField.TextChanged += new TextChangedEventHandler(TextBox_TextChanged);
            }
        }
    }
}