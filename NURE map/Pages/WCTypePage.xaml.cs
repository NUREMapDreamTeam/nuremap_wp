﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace NURE_map.Pages
{
    public partial class WCTypePage : PhoneApplicationPage
    {
        public WCTypePage()
        {
            InitializeComponent();
        }

        private void LayoutRoot_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            double x = e.GetPosition(LayoutRoot).X;
            if (x>LayoutRoot.RenderSize.Width/2)
                global.path = PathCreator.GetPath(global.from.VertexID, 'm');
            else
                global.path = PathCreator.GetPath(global.from.VertexID, 'f');
            NavigationService.Navigate(new Uri("/Pages/PathView.xaml", UriKind.Relative));
        }
    }
}