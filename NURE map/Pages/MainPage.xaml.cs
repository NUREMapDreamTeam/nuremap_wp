﻿using System;
using System.Windows;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;

namespace NURE_map
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();
            //var x = DateTime.Now;
            Graph.init();
            //var y = DateTime.Now;
            //MessageBox.Show((y - x).Duration().ToString());
        }
        bool c = true;
        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (global.from == null)
            {
                if (c)
                    NavigationService.Navigate(new Uri("/Pages/FromSelect.xaml", UriKind.Relative));
                c = false;
            }
            else
                SelectButton.Content = global.from.Name;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            while (NavigationService.RemoveBackEntry() != null) ;    //clear back stack
        }

        private void classroomBtn_Click(object sender, RoutedEventArgs e)
        {
            if (global.from == null)
            {
                MessageBox.Show("сначала нужно указать, где вы находитесь");
                return;
            }
            NavigationService.Navigate(new Uri("/Pages/ToSelect.xaml", UriKind.Relative));
        }

        private void Button_Free(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/FreeView.xaml", UriKind.Relative));
        }

        private void toiletButton_Click(object sender, RoutedEventArgs e)
        {
            if (global.from == null)
            {
                MessageBox.Show("сначала нужно указать, где вы находитесь");
                return;
            }
            NavigationService.Navigate(new Uri("/Pages/WCTypePage.xaml", UriKind.Relative));
            //if (MessageBox.Show("мужской / женский", "какой?", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            //    global.path = PathCreator.GetPath(global.from.VertexID,'m');
            //else
            //   global.path = PathCreator.GetPath(global.from.VertexID,'f');
            //NavigationService.Navigate(new Uri("/Pages/PathView.xaml", UriKind.Relative));
        }

        private void SelectLocation_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/FromSelect.xaml", UriKind.Relative));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (global.from == null)
            {
                MessageBox.Show("сначала нужно указать, где вы находитесь");
                return;
            }
            global.path = PathCreator.GetPath(global.from.VertexID, 'e');
            NavigationService.Navigate(new Uri("/Pages/PathView.xaml", UriKind.Relative));
        }
    }
}