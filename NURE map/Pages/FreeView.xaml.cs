﻿using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Input;

namespace NURE_map
{
    public partial class FreeView : PhoneApplicationPage
    {
        List<Path.Location> corps = new List<Path.Location>();
        int currentLocID = 0;
        public FreeView()
        {
            InitializeComponent();

            {
                corps.Add(new Path.Location("main", 4));
                corps.Add(new Path.Location("z", 5));
                corps.Add(new Path.Location("i", 6));
            }
            update();
        }

        int floor = 1;
        private void up_Click(object sender, RoutedEventArgs e)
        {
            if (floor < corps[currentLocID].Floor) ++floor;
            update();
        }

        private void down_Click(object sender, RoutedEventArgs e)
        {
            if (floor > 1) --floor;
            update();
        }
        void update()
        {
            mapImage.Source = new BitmapImage(new Uri("/MapsImg/"+floor+corps[currentLocID].Corps+".png", UriKind.RelativeOrAbsolute));
        }

        private void left_Click(object sender, RoutedEventArgs e)
        {
            floor = 1;
            currentLocID += corps.Count;
            --currentLocID;
            currentLocID %= corps.Count;
            update();
        }

        private void right_Click(object sender, RoutedEventArgs e)
        {
            floor = 1;
            ++currentLocID;
            currentLocID %= corps.Count;
            update();
        }

        const double MaxScale = 10;

        double _scale;
        double _minScale;
        double _coercedScale;
        double _originalScale;

        Size _viewportSize;
        bool _pinching;
        Point _screenMidpoint;
        Point _relativeMidpoint;

        BitmapImage _bitmap;

        void viewport_ViewportChanged(object sender, System.Windows.Controls.Primitives.ViewportChangedEventArgs e)
        {
            Size newSize = new Size(viewport.Viewport.Width, viewport.Viewport.Height);
            if (newSize != _viewportSize)
            {
                _viewportSize = newSize;
                CoerceScale(true);
                ResizeImage(false);
            }
        }
        void OnManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            _pinching = false;
            _originalScale = _scale;
        }
        void OnManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (e.PinchManipulation != null)
            {
                e.Handled = true;

                if (!_pinching)
                {
                    _pinching = true;
                    Point center = e.PinchManipulation.Original.Center;
                    _relativeMidpoint = new Point(center.X / mapImage.ActualWidth, center.Y / mapImage.ActualHeight);

                    var xform = mapImage.TransformToVisual(viewport);
                    _screenMidpoint = xform.Transform(center);
                }

                _scale = _originalScale * e.PinchManipulation.CumulativeScale;

                CoerceScale(false);
                ResizeImage(false);
            }
            else if (_pinching)
            {
                _pinching = false;
                _originalScale = _scale = _coercedScale;
            }
        }

        void OnManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            _pinching = false;
            _scale = _coercedScale;
        }

        void OnImageOpened(object sender, RoutedEventArgs e)
        {
            _bitmap = (BitmapImage)mapImage.Source;

            // Set scale to the minimum, and then save it. 
            _scale = 0;
            CoerceScale(true);
            _scale = _coercedScale;

            ResizeImage(true);
            viewport.SetViewportOrigin(
                       new Point(0, 0));
        }

        void ResizeImage(bool center)
        {
            if (_coercedScale != 0 && _bitmap != null)
            {
                double newWidth = canvas.Width = Math.Round(_bitmap.PixelWidth * _coercedScale);
                double newHeight = canvas.Height = Math.Round(_bitmap.PixelHeight * _coercedScale);

                xform.ScaleX = xform.ScaleY = _coercedScale;

                viewport.Bounds = new Rect(0, 0, newWidth, newHeight);

                if (center)
                {
                    viewport.SetViewportOrigin(
                        new Point(
                            Math.Round((newWidth - viewport.ActualWidth) / 2),
                            Math.Round((newHeight - viewport.ActualHeight) / 2)
                            ));
                }
                else
                {
                    Point newImgMid = new Point(newWidth * _relativeMidpoint.X, newHeight * _relativeMidpoint.Y);
                    Point origin = new Point(newImgMid.X - _screenMidpoint.X, newImgMid.Y - _screenMidpoint.Y);
                    viewport.SetViewportOrigin(origin);
                }
            }
        }

        void CoerceScale(bool recompute)    //set min scale
        {
            if (recompute && _bitmap != null && viewport != null)
            {
                // Calculate the minimum scale to fit the viewport 
                double minX = viewport.ActualWidth / _bitmap.PixelWidth;
                double minY = viewport.ActualHeight / _bitmap.PixelHeight;

                _minScale = Math.Min(minX, minY);
            }

            _coercedScale = Math.Min(MaxScale, Math.Max(_scale, _minScale));    // minscale < scale < maxscale

        }
    }
}