﻿using System.Windows;

namespace NURE_map
{
    class VertexCreator
    {
        internal static Vertex Create(string s) //name x y corps floor *neighbors
        {
            var vertex = new Vertex();
            var x = s.Split(' ');
            vertex.name = int.Parse(x[0]);
            vertex.coords = new Point(int.Parse(x[1]), int.Parse(x[2]));
            vertex.corp = x[3];
            vertex.floor = int.Parse(x[4]);
            return vertex;
        }
    }
}
