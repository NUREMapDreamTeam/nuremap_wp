﻿using System.Windows;
using System.Collections.Generic;
using System;
namespace NURE_map
{
    public class Vertex
    {
        public int name { get; set; }
        public Vertex[] neighbors {get;set;}
        public int floor { get; set; }
        public string corp { get; set; }
        public Point coords { get; set; }
        public Vertex(){}
    }
}
