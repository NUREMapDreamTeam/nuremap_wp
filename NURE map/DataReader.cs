﻿using System;
using System.Collections.Generic;
using System.IO;

namespace NURE_map
{
    static class DataReader
    {
        private static Vertex Search(List<Vertex> v, int n)
        {
            foreach (var i in v)
                if (i.name == n) return i;
            return null;
        }
        public static List<Vertex> LoadGraph(string filename)
        {
            var v = new List<Vertex>();
            using (TextReader tr = File.OpenText(filename))
            {
                string s;
                while ((s = tr.ReadLine()) != null)
                {
                    v.Add(VertexCreator.Create(s));
                }
            }
            using (TextReader tr = File.OpenText(filename))
            {
                string s;
                while ((s = tr.ReadLine()) != null)
                {
                    int start = 5;
                    var t = s.Split();
                    var el = Search(v, Convert.ToInt32(t[0]));
                    el.neighbors = new Vertex[t.Length - start];
                    for (int i = start; i < t.Length; ++i)
                    {
                        el.neighbors[i - start] = Search(v, Convert.ToInt32(t[i]));
                    }

                }
            }
            return v;
        }
        public static Dictionary<string, Dictionary<string, ClassRoom>> ReadNames(string filename)
        {
            var auds = new Dictionary<string, Dictionary<string, ClassRoom>>();
            auds.Add("main", new Dictionary<string, ClassRoom>());
            auds["main"].Add("Главный вход", new ClassRoom("Главный вход", 3, ""));
            using (TextReader tr = File.OpenText(filename))
            {
                string s;
                int i = 0;
                while ((s = tr.ReadLine()) != null)
                {
                    var t = s.Split('|');
                    int vertexID = Int32.Parse(t[1]);
                    var corp = Graph.verteces[vertexID].corp;
                    if (!auds.ContainsKey(corp))
                        auds.Add(corp, new Dictionary<string,ClassRoom>());
                    auds[corp].Add(t[0],new ClassRoom(t[0],vertexID,t[2]));
                    ++i;
                }
            }
            return auds;
        }
        public static Place[] addPlaces(string filename)
        {
            using (TextReader tr = File.OpenText(filename))
            {
                string s, s1, c;
                s = tr.ReadLine();
                int n = int.Parse(s);
                Place[] places = new Place[n];
                for (int i = 0; i < n; ++i)
                {
                    s = tr.ReadLine();
                    c = tr.ReadLine();
                    s1 = tr.ReadLine();
                    places[i] = new Place(s[0], c, s1);
                }
                return places;
            }
        }
    }
}
