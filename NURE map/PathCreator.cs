﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NURE_map
{
    class PathCreator
    {
        private static int PathLength(List<Vertex> list)
        {
            if (list == null) return 10000000;
            int r = 0;
            var last = list[0];
            foreach (var i in list)
            {
                ++r;
                r += (int)(Math.Abs(i.coords.X - last.coords.X) + Math.Abs(i.coords.Y - last.coords.Y));
                if (last.floor != i.floor) r += 32;
                last = i;
            }
            return r;
        }
        public static Path GetPath(int from, int to)
        {
            var p = Deijkstr(from);
            //recover path
            List<Vertex> res = new List<Vertex>();
            while (to > 0)
            {
                res.Add(Graph.verteces[to]);
                to = p[to];
            }
            res.Reverse();
            return new Path(res);
        }
        public static Path GetPath(int from, char pl)
        {
            List<Vertex> res = null;
            var p = Deijkstr(from);
            for (int i = 0; i < Graph.places.Length; ++i)
            {
                List<Vertex> t = new List<Vertex>();
                if (Graph.places[i].name != pl) continue;
                int to = Graph.classrooms[Graph.places[i].corps][Graph.places[i].location].VertexID;   // need support of other corps!
                while (to > 0)
                {
                    t.Add(Graph.verteces[to]);
                    to = p[to];
                }
                if (PathLength(t) < PathLength(res))
                    res = t;
            }
            res.Reverse();
            return new Path(res);
        }
        public static int[] Deijkstr(int start)
        {
            List<Vertex> verteces = Graph.verteces;
            int vn = verteces.Count;
            bool[] Labels = new bool[vn];
            int[] Distances = new int[vn];
            int[] from = new int[vn];

            //fill first values to infinity
            for (int i = 0; i < Distances.Length; ++i)
            {
                Distances[i] = 1000000;
                from[i] = -1;
                Labels[i] = true;
            }

            Distances[start] = 0;
            while (true)
            {
                for (int i = 0; i < vn; ++i)
                {
                    
                    int len = (int)(Math.Abs(verteces[i].coords.X - verteces[start].coords.X) + 
                        Math.Abs(verteces[i].coords.Y - verteces[start].coords.Y));
                    if (verteces[i].floor != verteces[start].floor || verteces[i].corp != verteces[start].corp) len = 64;   //32
                    if (Labels[i] && verteces[start].neighbors.Contains(verteces[i]) && Distances[i] > Distances[start] + len)
                    {
                        Distances[i] = Distances[start] + ( verteces[i].corp!="z"?len*3:len);
                        from[i] = start;
                    }
                }
                Labels[start] = false;
                start = -1;
                int minDistance = 10000000, minID = -1;
                for (int i = 1; i < vn; ++i)
                {
                    if (Labels[i] && Distances[i] < minDistance)
                    {
                        minDistance = Distances[i];
                        minID = i;
                    }

                }
                start = minID;
                if (start == -1) break;
            }
            return from;
        }
    }
}
