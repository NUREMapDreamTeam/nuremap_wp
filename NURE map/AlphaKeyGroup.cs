﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.Phone.Globalization;

namespace NURE_map
{
    public class AlphaKeyGroup<T> : List<T> where T:ClassRoom
    {
        
        public delegate string GetKeyDelegate(T item);
        public string Key { get; private set; }
        public AlphaKeyGroup(string key)
        {
            Key = key;
        }
        public static List<AlphaKeyGroup<T>> CreateGroups(IEnumerable<T> items, GetKeyDelegate getKey, bool sort)
        {
            List<AlphaKeyGroup<T>> list = new List<AlphaKeyGroup<T>>();
            list.Add(new AlphaKeyGroup<T>("Главный корпус"));
            list.Add(new AlphaKeyGroup<T>("Корпус З"));
            list.Add(new AlphaKeyGroup<T>("Корпус И"));

            foreach (T item in items)
            {
                int index = 0;
                switch (Graph.verteces[item.VertexID].corp)
                {
                    case "z":
                        index = 1;
                        break;
                    case "i":
                        index = 2;
                        break;
                }
                list[index].Add(item);
            }

            if (sort)
                foreach (AlphaKeyGroup<T> group in list)
                    group.Sort((c0, c1) => { return Compare(c0.Name, c1.Name); });
            return list;
        }

        private static int Compare(string p1, string p2)
        {
            for (int i = 0; i < p1.Length && i < p2.Length; ++i)
            {
                if (p1[i] != p2[i])
                    if (char.IsDigit(p1[i]) == (!char.IsDigit(p2[i])))
                        return char.IsDigit(p1[i]) ? 1 : -1;
                    else
                        return p1[i].CompareTo(p2[i]);
            }
            return p1.Length-p2.Length;
        }

    }
}