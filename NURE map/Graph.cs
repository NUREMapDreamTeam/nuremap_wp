﻿using System;
using System.Collections.Generic;

namespace NURE_map
{
    static class Graph
    {
        public static Place[] places;
        public static List<Vertex> verteces;
        public static Dictionary<string, Dictionary<string, ClassRoom>> classrooms;
        public static void init()
        {
            verteces = new List<Vertex>();
            verteces = DataReader.LoadGraph("Resources/graph.txt");
            classrooms = DataReader.ReadNames("Resources/classrooms.txt");
            places = DataReader.addPlaces("Resources/places.txt");
            //BuildingStructure.init();
        }
    }
}
