﻿namespace NURE_map
{
    public class Place
    {
        public char name;
        public string corps;
        public string location;
        public Place(char n, string c, string l)
        {
            name = n;
            corps = c;
            location = l;
        }
    }
}
