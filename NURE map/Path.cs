﻿using System.Collections.Generic;
using System.Windows.Media;

namespace NURE_map
{
    class Path
    {
        public class Location
        {
            public string Corps;
            public int Floor;
            public Location(string corp, int floor)
            {
                this.Corps = corp;
                this.Floor = floor;
            }
        }
        public List<Vertex> v;
        public int nfloors = 1;
        public Location[] parts;
        public System.Windows.Media.PointCollection[] lpoints;
        public Path(List<Vertex> v)
        {
            this.v = v;
            Location lastl = new Location(v[0].corp, v[0].floor);
            foreach (var i in v)
                if (lastl.Floor != i.floor||lastl.Corps!=i.corp)
                {
                    lastl = new Location(i.corp, i.floor);
                    ++nfloors;
                }
            parts = new Location[nfloors];
            lastl = new Location(v[0].corp, v[0].floor);
            int j = 0;
            parts[j] = lastl;
            lpoints = new PointCollection[nfloors];
            for (int i = 0; i < nfloors; ++i)
                lpoints[i] = new PointCollection();
            foreach (var i in v)
            {
                if (lastl.Floor != i.floor || lastl.Corps != i.corp)
                {
                    ++j;
                    lastl = new Location(i.corp, i.floor);
                    parts[j] = lastl;
                }
                lpoints[j].Add(i.coords);
            }
        }
    }
}
